ifeq ($(OS),Windows_NT)
	Linux=no
	UNAME_S = Windows_NT
else
	UNAME_S=$(shell uname -s)
endif

ifeq ($(UNAME_S),Linux)
	Linux=yes
endif

DirSrc=src
DirSrcClient=$(DirSrc)/client
DirSrcServeur=$(DirSrc)/serveur
DirObj=obj

Compiler=gcc
Link=-o
Preproc=-c -g -O3
PreprocFlags=
LinkFlags=$(PreprocFlags)

ifeq ($(Linux),yes)
	PreprocSysFlags=
	LinkSysFlags=$(PreprocSysFlags)
	RM=rm -f
	cmdClean=$(RM) $(DirObj)/*.o
	Extension=
else
	PreprocSysFlags=
	LinkSysFlags=$(PreprocSysFlags) -lwsock32
	RM=del
	cmdClean=$(RM) $(DirObj)\*.o
	Extension=.exe
endif

Src=$(wildcard $(DirSrc)/*.c)
SrcClient=$(wildcard $(DirSrcClient)/*.c)
SrcServeur=$(wildcard $(DirSrcServeur)/*.c)

Obj=$(patsubst $(DirSrc)/%.c,$(DirObj)/%.o,$(Src))
ObjClient=$(patsubst $(DirSrcClient)/%.c,$(DirObj)/%.o,$(SrcClient))
ObjServeur=$(patsubst $(DirSrcServeur)/%.c,$(DirObj)/%.o,$(SrcServeur))

ExecClient=client$(Extension)
ExecServeur=serveur$(Extension)

all: $(DirObj) $(ExecClient) $(ExecServeur)
default: $(DirObj) $(ExecClient) $(ExecServeur)

$(ExecClient): $(Obj) $(ObjClient)
	$(Compiler) $(Link) $@ $^ $(LinkFlags) $(LinkSysFlags)

$(ExecServeur): $(Obj) $(ObjServeur)
	$(Compiler) $(Link) $@ $^ $(LinkFlags) $(LinkSysFlags)

$(DirObj)/%.o: $(DirSrc)/%.c | $(DirObj)
	$(Compiler) $(Link) $@ $< $(Preproc) $(PreprocFlags) $(PreprocSysFlags)

$(DirObj)/%.o: $(DirSrcClient)/%.c | $(DirObj)
	$(Compiler) $(Link) $@ $< $(Preproc) $(PreprocFlags) $(PreprocSysFlags)

$(DirObj)/%.o: $(DirSrcServeur)/%.c | $(DirObj)
	$(Compiler) $(Link) $@ $< $(Preproc) $(PreprocFlags) $(PreprocSysFlags)

$(DirObj):
	mkdir $@

.PONY: clean mrproper run

clean:
	$(cmdClean)

mrproper:
	$(RM) $(ExecClient) $(ExecServeur)