#include "../include/Portabilite.h"

// Code source copié du site https://broux.developpez.com/articles/c/sockets/#LII-C

void init(void)
{
	#ifdef WIN32
		WSADATA wsa;
		int err = WSAStartup(MAKEWORD(2, 2), &wsa);
		if(err < 0)
		{
			puts("WSAStartup failed !");
			exit(EXIT_FAILURE);
		}
	#endif
}

void end(void)
{
	#ifdef WIN32
		WSACleanup();
	#endif
}